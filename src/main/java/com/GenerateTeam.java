package com;

import java.io.InputStream;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.Random;

import com.domain.Person;
import com.domain.Response;
import com.filter.CorsFilter;
import com.google.gson.Gson;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;

import static spark.Spark.*;

public class GenerateTeam {
	
	public static void main(String[] args) {
		
		
		 ProcessBuilder process = new ProcessBuilder(); 
		    Integer port;
		    // This tells our app that if Heroku sets a port for us, we need to use that port.
		    // Otherwise, if they do not, continue using port 4567.
		    if (process.environment().get("PORT") != null) {
		        port = Integer.parseInt(process.environment().get("PORT"));
		    } else {
		        port = 4567;
		    }
		    System.out.println("PORT->"+port);
		    port(port);
		    
//		    CorsFilter.apply();
		Gson gson = new Gson();
		
		options("/*", (req, res) -> {
			String accessControlRequestHeaders = req.headers("Access-Control-Request-Headers");
			if (accessControlRequestHeaders != null) {
				res.header("Access-Control-Allow-Headers", accessControlRequestHeaders);
			}

			String accessControlRequestMethod = req.headers("Access-Control-Request-Method");
			if (accessControlRequestMethod != null) {
				res.header("Access-Control-Allow-Methods", accessControlRequestMethod);
			}

	        return "OK";
		});
		
		before((req, res) -> {
			res.header("Access-Control-Allow-Origin", "*");
			res.header("Access-Control-Allow-Headers", "*");
			res.type("application/json");
		});
		
		post("/get-teams", (request, response) -> {
			String requestStr = request.body();
			
			JsonElement json = gson.fromJson(requestStr,JsonElement.class);
			JsonObject requestObj = json.getAsJsonObject();
			String teams = requestObj.get("teams").getAsString();
			String groups = requestObj.get("groups").getAsString();
			
			
			
			 response.type("application/json");
			 Map<Integer, List<Person>> teamsListmap = new GenerateTeam().getTeam(teams,groups);
			 List<List<String>> teamList = new ArrayList<>();
			 for(int i : teamsListmap.keySet()){
				 List<String> group = new ArrayList<>();
				 for(Person p : teamsListmap.get(i)){
					 group.add(p.getName());
				 }
				 teamList.add(group);
			 }
			 Response resp = new Response();
			 resp.setTeamList(teamList);
	            return gson.toJson(resp);
		});
	        System.out.println();
	        
	}
	
//	public static void main(String[] args) {
////		new GenerateTeam().getTeam();
//		
//	        
//	        System.out.println("hi there");
//	}
	public Map<Integer, List<Person>> getTeam(String input,String groups){
		int N = Integer.valueOf(groups);
		String[]  inputArray = input.split(",");
		List<String> lit = new ArrayList<String>(Arrays.asList(inputArray));
		Collections.shuffle(lit);
		System.out.println(lit);
		int max = inputArray.length;
		List<Person> teamMembers = new ArrayList<Person>();
		for(String name: inputArray){
			Person member = new Person();
			member.setName(name);
			member.setNumber(GenerateTeam.randInt(1, max+1)+1);
			teamMembers.add(member);
		}
		
		Collections.sort(teamMembers, new Comparator<Person>() {

			@Override
			public int compare(Person o1, Person o2) {
				// TODO Auto-generated method stub
				return o1.getNumber().compareTo(o2.getNumber());
			}
			
		});
		Map<Integer, List<Person>> teams = printTeamMembers(teamMembers,N);
		return teams;
		
	}
	
	private Map<Integer, List<Person>> printTeamMembers(List<Person> teamMembers,int noOfTeams) {
		
		Map<Integer, List<Person>> teams = new LinkedHashMap<>();
		int lastMember = teamMembers.size();
		System.out.println(lastMember);
	for (int i = 0; i < noOfTeams;i=i+1){
	
		List<Person> team = new ArrayList<Person>();
		for(int j = i;j<lastMember;j=j+noOfTeams){
			team.add(teamMembers.get(j));
		}
		
		teams.put(i, team);
	}
	for(Integer key : teams.keySet()){
		for(Person p : teams.get(key)){
			System.out.print(p.getName()+",");
		}
		System.out.println("--");
	}
	
	return teams;
	}
	public static int randInt(int min, int max) {

	    // Usually this can be a field rather than a method variable
	    Random rand = new Random();

	    // nextInt is normally exclusive of the top value,
	    // so add 1 to make it inclusive
	    int randomNum = rand.nextInt((max - min)+1) + min;

	    return randomNum;
	}

}
